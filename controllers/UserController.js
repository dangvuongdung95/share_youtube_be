const router = require('express').Router();
const validators = require('../middleware/validators');

router.post('/login', [
    validators.valueRequired({ attributes: ['email', 'password'] }),
    validators.validEmail({ attributes: ['email'] })
], (req, res) => {
    (async () => {

        return await userService.login(null, req.body);
    })()
        .then((result) => {
            res.json(result);
        })
        .catch((err) => {
            log.error('login', err);
            res.json(jsonError(errors.SYSTEM_ERROR));
        });
});

router.post('/register', [
    validators.valueRequired({ attributes: ['email', 'password'] }),
    validators.validEmail({ attributes: ['email'] })
], (req, res) => {
    (async () => {
        return await userService.register(req.principal, req.body);
    })()
        .then((result) => {
            res.json(result);
        })
        .catch((err) => {
            log.error('register', err);
            res.json(jsonError(errors.SYSTEM_ERROR));
        });
});

module.exports = {
    mount: '/api',
    router
};
