const router = require('express').Router();
const validators = require('../middleware/validators');
const policies = require('../middleware/policies');

router.post('/share', [
    policies.authenticated(),
    validators.valueRequired({ attributes: ['url'] }),
], (req, res) => {
    (async () => {
        return await dataService.createData(req.principal, req.body);
    })()
        .then((result) => {
            res.json(result);
        })
        .catch((err) => {
            log.error('create_data', err);
            res.json(jsonError(errors.SYSTEM_ERROR));
        });
});

router.post('/get_data', [
], (req, res) => {
    (async () => {
        return await dataService.getAllData(req.principal, req.body);
    })()
        .then((result) => {
            res.json(result);
        })
        .catch((err) => {
            log.error('get_data', err);
            res.json(jsonError(errors.SYSTEM_ERROR));
        });
});


module.exports = {
    mount: '/api',
    router
};
