//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../app');
let should = chai.should();
chai.use(chaiHttp);
//Our parent block
describe('Events', () => {
    beforeEach((done) => {
        //Before each test we empty the database in your case
        done();
    });
    /*
     * Test the /GET route
     */
    describe('/GET shares', () => {
        it('it should GET all the shares', (done) => {
            chai.request('http://localhost:1995/api')
                .get('/shares')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(9);
                    done();
                });
        });
    });

    /*
     * Test the /POST route
     */
    describe('/POST shares', () => {
        it('it should POST a share', (done) => {
            let share = {
                url: 'https://www.youtube.com/watch?v=8TxJhLfsjC8',
            };
            chai.request(server)
                .post('/shares')
                .send(share)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);
                    done();
                });
        });
        it('it should not POST a book without status field', (done) => {
            let share = {
                url: 'https://www.youtussssbe.com/watch?v=8TxJhLfsjC8',
            };
            chai.request(server)
                .post('/shares')
                .send(share)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Wrong URL youtube error!');
                    done();
                });
        });
    });

    /*
     * Test the /POST route
     */
    describe('/POST login', () => {
        it('it should POST a login', (done) => {
            let data = {
                email: 'dungdv@gmail.com',
                password: '123123',
            };
            chai.request(server)
                .post('/login')
                .send(data)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);
                    done();
                });
        });
    });
       /*
     * Test the /POST route
     */
    describe('/POST register', () => {
        it('it should POST a register', (done) => {
            let data = {
                email: 'dungdv@gmail.com',
                password: '123123',
            };
            chai.request(server)
                .post('/register')
                .send(data)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);
                    done();
                });
        });
    });
});