# SHARE_YOUTUBE_BE

This is back-end of project. Writed by Node js, use MongoDB to store database.


***RUN PROJECT***
- Make sure install Node Js environment and MongoDB.
- copy env virable in example.env to new file lcl.env in env folder
- cd to project, run npm install
- npm start.


***GEN API DOC***
- Run command: npm i -g api-doc to install api-doc.
- cd to doc folder in project and run file gen-doc.sh.
- Open file htlm in .../doc/dist to view api doc.


