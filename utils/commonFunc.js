const request = require('request');

const isValidObjectId = (id) => {
	return id.match(/^[0-9a-fA-F]{24}$/) ? id : null;
};

const youtubeLinkParser = (url) => {
	const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
	const match = url.match(regExp);
	if (match && match[2].length == 11) {
		return jsonSuccess(match[2]);
	} else {
		return jsonError();;
	}
}

const getDataFromYoutube = async (id) => {
	return await new Promise((resolve) => {
		request(`https://www.googleapis.com/youtube/v3/videos?part=snippet&id=${id}&fields=items/snippet/title,items/snippet/description&key=${getEnv('API_GOOGLE_KEY')}`, function (err, response, body) {
			if (err) {
				log.error(err);
				return resolve(jsonError(errors.SYSTEM_ERROR));
			}
			return resolve(jsonSuccess(JSON.parse(body)));
		});
	});
};

module.exports = { isValidObjectId, youtubeLinkParser, getDataFromYoutube };