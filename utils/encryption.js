const bcrypt = require('bcrypt');

const hashPassword = async (password) => {
	let saltResult = await new Promise((resolve) => {
		bcrypt.genSalt(Number(getEnv('SALT_ROUND')), (err, salt) => {
			if (err) {
				log.error(err);
				return resolve(jsonError(errors.SYSTEM_ERROR));
			}
			return resolve(jsonSuccess(salt));
		});
	});
	
	if (!saltResult.success) return saltResult;
	
	return await new Promise((resolve) => {
		bcrypt.hash(password, saltResult.result, (err, hash) => {
			if (err) {
				log.error(err);
				return resolve(jsonError(errors.SYSTEM_ERROR));
			}
			return resolve(jsonSuccess(hash));
		});
	});
};

const comparePassword = async (password, passwordHash) => {
	return await new Promise((resolve) => {
		bcrypt.compare(password, passwordHash, (err, result) => {
			if (err) {
				log.error(err);
				return resolve(jsonError(errors.SYSTEM_ERROR));
			}
			return resolve(jsonSuccess(result));
		});
	});
};

module.exports = {hashPassword, comparePassword};
