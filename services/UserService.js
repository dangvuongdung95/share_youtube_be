const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const encryption = require("../utils/encryption");

class UserService {
    async register(principal, params) {
        const {email, password} = params;

        // check email exitst
        const user = await User.findOne({email}).lean();
        if (user) return jsonError(errors.DUPLICATED_EMAIL_ERROR);
        // hash password
        const passwordHash = await encryption.hashPassword(password);
        if (!passwordHash.success) return passwordHash;
        // create new user
        const newUser = new User({
            email,
            password: passwordHash.result,
        });
        await newUser.save();

        const tokenResult = await new Promise(resolve => {
            jwt.sign(
                {_id: newUser._id},
                getEnv("JWT_SECRET"),
                {expiresIn: getEnv("JWT_LOGIN_EXPIRED_IN")},
                (err, token) => {
                    if (err) {
                        log.error("UserService/register/tokenGenerate", err);
                        return resolve(jsonError(errors.SYSTEM_ERROR));
                    }
                    return resolve(jsonSuccess(token));
                }
            );
        });
        if (!tokenResult.success) return tokenResult;
        return jsonSuccess({email, token: tokenResult.result});
    }

    async login(principal, params) {
        const {email, password} = params;

        //-- check email
        const user = await User.findOne({email}).lean();
        if (!user)
            return jsonError(errors.INVALID_CREDENTIAL);

        //-- check the password
        const passwordCheckResult = await new Promise(resolve => {
            bcrypt.compare(password, user.password, (err, matched) => {
                if (err) {
                    log.error("UserService/login/passwordCheck", err);
                    return resolve(jsonError(errors.SYSTEM_ERROR));
                }
                if (!matched) {
                    return resolve(jsonError(errors.INVALID_CREDENTIAL));
                }
                return resolve(jsonSuccess());
            });
        });

        if (!passwordCheckResult.success)
            return passwordCheckResult;

        //-- generate token
        const tokenResult = await new Promise(resolve => {
            jwt.sign(
                {_id: user._id},
                getEnv("JWT_SECRET"),
                {expiresIn: getEnv("JWT_LOGIN_EXPIRED_IN")},
                (err, token) => {
                    if (err) {
                        log.error("UserService/login/tokenGenerate", err);
                        return resolve(jsonError(errors.SYSTEM_ERROR));
                    }
                    return resolve(jsonSuccess(token));
                }
            );
        });
        if (!tokenResult.success) return tokenResult;

        return jsonSuccess({
            email,
            token: tokenResult.result,
        });
    }

}

module.exports = UserService;
