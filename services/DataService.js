const commonFunc = require("../utils/commonFunc");

class DataService {
    async createData(principal, params) {
        const { url } = params;
        const userId = principal.user._id;

        const Regex = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/;
        if (!url.match(Regex)) return jsonError(errors.WRONG_URL_ERROR);

        const youtubeId = commonFunc.youtubeLinkParser(url);
        if (!youtubeId.success) return youtubeId

        const dataYoutube = await commonFunc.getDataFromYoutube(youtubeId.result);
        if (!dataYoutube.success) return dataYoutube

        const { description, title } = dataYoutube.result.items && dataYoutube.result.items[0] && dataYoutube.result.items[0].snippet

        const data = new Data({ url, userId, description, title });
        await data.save();

        return jsonSuccess();
    }

    async getAllData(principal, params) {
        let { page, limit } = params;

        page = !page ? 1 : page;
        limit = !limit ? 10 : limit;

        const data = await Data.paginate(
            {},
            {
                sort: { "$natural": -1 },
                page,
                limit,
                populate: [{ path: 'userId', select: 'email' }],
                lean: true
            }
        );
        return jsonSuccess(data);
    }
}

module.exports = DataService;
