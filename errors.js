module.exports = {
    //-- system errors
    'SYSTEM_ERROR': { code: 'SYSTEM_ERROR', mess: 'Something wrong in server!!!' },
    'ENV_NOT_SET_ERROR': { code: 'ENV_NOT_SET_ERROR' },
    'NOT_IMPLEMENTED_ERROR': { code: 'NOT_IMPLEMENTED_ERROR' },
    'SERVER_SHUTTING_DOWN': { code: 'SERVER_SHUTTING_DOWN' },
    'SERVICE_CHECK_FAILED': { code: 'SERVICE_CHECK_FAILED' },
    'PREBOOT_ERROR': { code: 'PREBOOT_ERROR' },
    'BOOT_ERROR': { code: 'BOOT_ERROR' },
    'PREEXIT_ERROR': { code: 'PREEXIT_ERROR' },
    'EXIT_ERROR': { code: 'EXIT_ERROR' },
    'LISTEN_ERROR': { code: 'LISTEN_ERROR' },
    'SERVICE_BOOT_FAILED': { code: 'SERVICE_BOOT_FAILED' },

    //-- user-defined errors
    'MISSING_REQUIRED_VALUE': { code: 'MISSING_REQUIRED_VALUE', mess: 'Missing params required!' },
    'NOT_AUTHENTICATED_ERROR': { code: 'NOT_AUTHENTICATED_ERROR', mess: 'Not authenticated error!' },
    'NOT_VALID_ID': { code: 'NOT_VALID_ID', mess: 'Params is not a valid _id!' },
    'DUPLICATED_EMAIL_ERROR': { code: 'DUPLICATED_EMAIL_ERROR', mess: 'Duplicated email error!' },
    'INVALID_CREDENTIAL': { code: 'INVALID_CREDENTIAL', mess: 'Email or password is wrong!'  },
    'NOT_VALID_EMAIL_VALUE': { code: 'NOT_VALID_EMAIL_VALUE', mess: "Not valid email value!" },
    'WRONG_URL_ERROR': { code: 'WRONG_URL_ERROR' , mess: 'Wrong URL youtube error!' },
};


