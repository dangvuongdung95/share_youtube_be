

/**
 * @apiDefine AdminOnly Admin role required
 * User must login as admin to perform this action
 */

// ======================= SERVER SETTINGS =============================
/**
 * @api {CONFIG} Server-Config
 * @apiGroup Settings
 * @apiVersion 1.0.0
 *
 * @apiDescription
 * These settings below are applied for current API version
 *
 * @apiParam {String} API-MOUNT-POINT <code>/api</code>
 * @apiParam {String} Access-Control-Allow-Origin <code>'*'</code>
 * @apiParam {String} Access-Control-Allow-Methods GET, POST, OPTIONS
 */


// ======================= AUTH =============================

/**
 * @api {POST} /login Login
 * @apiName Login
 * @apiGroup AUTH
 * @apiVersion 1.0.0

 * @apiDescription
 * User login
 *
 * @apiParam {String} email Email of user
 * @apiParam {String} password Password to login
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {String} result.token Access token
 * @apiSuccess {String} result.email Email of user
 *
 * @apiSuccessExample {json} Success-Response:
 * {
 *     "success": true,
 *     "result": {
 *         "email": "dangvuongdung95@gmail.com",
 *         "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZDU1MmE4ZGU4MWRjNjkyNDg4MzQxMmMiLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE1NjYyODM0OTUsImV4cCI6MTU2NjM2OTg5NX0.623-6T5akjQGHHW_9JVzeYrYH2npbNJ_Tlw7nf47RFs"
 *     }
 * }
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code
 * - <code>'NOT_FOUND_ERROR'</code>: Admin not found
 * - <code>'INVALID_CREDENTIAL'</code>: Email or password is wrong
 *  @apiErrorExample {json} Error-Response:
 *     {
 *          "success": false,
 *          "error": {
 *              "code": "INVALID_CREDENTIAL"
 *          }    
 *     }
 */
/**
 * @api {POST} /register User register
 * @apiName Register
 * @apiGroup AUTH
 * @apiVersion 1.0.0

 * @apiDescription
 * User register
 *
 * @apiParam {String} email Email of user
 * @apiParam {String} password Password to login
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {String} result.token Access token
 * @apiSuccess {String} result.email Email of user
 * 
 * @apiSuccessExample {json} Success-Response:
 * {
 *     "success": true,
 *     "result": {
 *         "email": "dangvuongdung95@gmail.com",
 *         "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZDU1MmE4ZGU4MWRjNjkyNDg4MzQxMmMiLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE1NjYyODM0OTUsImV4cCI6MTU2NjM2OTg5NX0.623-6T5akjQGHHW_9JVzeYrYH2npbNJ_Tlw7nf47RFs"
 *     }
 * }
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code
 * - <code>'DUPLICATED_EMAIL_ERROR'</code>: Email is duplicated
 *  @apiErrorExample {json} Error-Response:
 *     {
 *          "success": false,
 *          "error": {
 *              "code": "DUPLICATED_EMAIL_ERROR"
 *          }    
 *     }
 */


 /**
 * @api {POST} /share Share a movies
 * @apiName Share a movies
 * @apiGroup SHARE
 * @apiVersion 1.0.0

 * @apiDescription
 * Share a movies
 * 
 * @apiHeader {String} Authorization Admin access token.
 * 
 * @apiParam {String} url URL of youtube video
 *
 * @apiSuccess {Boolean} success <code>true</code>
 *
 * @apiSuccessExample {json} Success-Response:
 * {
 *     "success": true,
 * }
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code
 * - <code>'WRONG_URL_ERROR'</code>: URL is wrong
 *  @apiErrorExample {json} Error-Response:
 *     {
 *          "success": false,
 *          "error": {
 *              "code": "WRONG_URL_ERROR"
 *          }    
 *     }
 */

 /**
 * @api {POST} /get_data Get all movies share
 * @apiName Get all movies share
 * @apiGroup SHARE
 * @apiVersion 1.0.0

 * @apiDescription
 * Get all movies share
 *
 * @apiParam {Number} [limit] Limit videos in one page
 * @apiParam {Number} [page] Page of videos
 *
 * @apiSuccess {Boolean} success <code>true</code>
 * @apiSuccess {Object} result
 * @apiSuccess {Array} result.docs Data of videos
 * @apiSuccess {Number} result.total Total of videos
 * @apiSuccess {Number} result.limit Limit of videos
 * @apiSuccess {Number} result.page Page of videos
 * @apiSuccess {Number} result.pagse Pages of videos
 *
 * @apiSuccessExample {json} Success-Response:
 * {
 *    "success": true,
 *    "result": {
 *       "docs": [
 *           {
 *               "_id": "5d68c2399c505be4a6c9e9b5",
 *               "url": "https://www.youtube.com/watch?v=8TxJhLfsjC8",
 *               "userId": {
 *                   "_id": "5d68c2109c505be4a6c9e9b4",
 *                   "email": "tsada@yupmail.com"
 *               },
 *               "description": "\nHoà Vũ Văn\n2 months ago\nNếu ngày mai em rời xa anh \nAnh không biết sống thế nào đây \nCon tim anh nhói đau từng cơn \nAnh biết hạt mưa không ngừng rơi khi thấy em buồn. \n\nXin thời gian hãy trở lại đi \nAnh không muốn mất em người ơi \nCon tim anh nhói đau từng cơn \nAnh khóc vì anh biết mình sai \nTất cả là vì tại anh. ",
 *               "title": "ANH SAI RỒI - SƠN TÙNG M-TP | HƯƠNG LY COVER",
 *               "createdAt": "2019-08-30T06:29:13.207Z",
 *               "updatedAt": "2019-08-30T06:29:13.207Z",
 *               "__v": 0,
 *               "id": "5d68c2399c505be4a6c9e9b5"
 *           },
 *       ],
 *       "total": 16,
 *       "limit": 10,
 *       "page": 1,
 *       "pages": 2
 *   }
 * }
 * @apiError {Boolean} success <code>false</code>
 * @apiError {Object} error
 * @apiError {String} error.code
 * - <code>'NOT_FOUND_ERROR'</code>: Not found videos
 *  @apiErrorExample {json} Error-Response:
 *     {
 *          "success": false,
 *          "error": {
 *              "code": "NOT_FOUND_ERROR"
 *          }    
 *     }
 */