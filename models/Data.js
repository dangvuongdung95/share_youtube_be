const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const dataSchema = new mongoose.Schema({
    userId: {type: mongoose.Schema.ObjectId, ref: "User", required: true},
    url: {type: String, require: true},
    description: {type: String, require: true},
    title: {type: String, require: true},
}, {timestamps: true});
dataSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Data', dataSchema);
